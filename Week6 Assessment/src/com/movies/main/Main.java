// Main Class

package com.movies.main;
import java.util.Scanner;

public class Main {
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter your choice: ");
		
		System.out.println("1. Movies Coming");
		System.out.println("2. Movies in Theatres");
		System.out.println("3. Top Rated India");
		System.out.println("4. Top Rated Movies");
		
		int ch = sc.nextInt();
		
		switch(ch)
		{
		
		case 1:
		{
			return MoviesComing();
			break;
		}
		
		case 2:
		{
			return MoviesinTheater();
			break;
		}
		
		case 3:
		{
			return TopRatedIndia();
			break;
		}
		
		case 4:
		{
			return TopRatedMovies();
			break;
		}
		
		default:
		{
			System.out.println("You entered a wrong choice");
		}
		
		}
		
	}
}
