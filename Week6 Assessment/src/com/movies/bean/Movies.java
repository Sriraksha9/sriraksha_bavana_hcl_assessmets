// Bean class

package com.movies.bean;

public class Movies {

private int id;
private String title ;
private int year;
private int ratings;
private String storyline;

// Getters and Setters

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}

public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}

public int getRatings() {
	return ratings;
}
public void setRatings(int ratings) {
	this.ratings = ratings;
}

public String getStoryline() {
	return storyline;
}
public void setStoryline(String storyline) {
	this.storyline = storyline;
}

// toStringMethod

@Override
public String toString() {
	return "Movies [id=" + id + ", title=" + title + ", year=" + year + ", ratings=" + ratings + ", storyline="
			+ storyline + "]";
}

}
