// Factory Design Method

package factory;

interface Movies()
{
	public void create();
}

class MoviesComing
{
	return MoviesComing();
}

class MoviesinTheatres
{
	return MoviesinTheaters();
}

class TopRatedIndia
{
	return TopRatedIndia();
}

class TopRatedMovies
{
	return TopRatedMovies();
}
public class MoviesFactory {
	public static Movies getInstance(String type) {
		
		if(type.equalsIgnoreCase("MoviesComing")) {
			return MoviesComing();
		}
		else if(type.equalsIgnoreCase("MoviesinTheatres")) {
			return MoviesinTheatres();
		}
		else if(type.equalsIgnoreCase("TopRatedIndia")) {
			return TopRatedIndia();
		}
		else if(type.equalsIgnoreCase("TopRatedMovies")) {
				return TopRatedMovies();
		}
		else {
			return null;
		}
		
	}
}
