create database movies_sriraksha;
use movies_sriraksha;


/* Movies Comming */

create table movieNamesType1(ID int primary key,
Title varchar(100) ,
Year int,
Catagory varchar(100));

insert into movieNamesType1 values(1,'Game Night',2018,'Movies Comming');
insert into movieNamesType1 values(2,'Area X: Annihilation',2018,'Movies Comming');
insert into movieNamesType1 values(3,'Hannah',2017,'Movies Comming');
insert into movieNamesType1 values(4,'The Lodgers',2017,'Movies Comming');
insert into movieNamesType1 values(5,'Beast of Burden',2018,'Movies Comming');

select * from movieNamesType1;


/* Movies in Theatres */

create table movieNamesType2(ID int primary key,
Title varchar(100) ,
Year int,
Catagory varchar(100));

insert into movieNamesType2 values(1,'Black Panther',2018,'Movies in Theatres');
insert into movieNamesType2 values(2,'Grottmannen Dug',2018,'Movies in Theatres');
insert into movieNamesType2 values(3,'Aiyaary',2018,'Movies in Theatres');
insert into movieNamesType2 values(4,'Samson',2018,'Movies in Theatres');
insert into movieNamesType2 values(5,'Loveless',2017,'Movies in Theatres');

select * from movieNamesType2;


/* Top Rated India */
  
create table movieNamesType3(ID int primary key,
Title varchar(100) ,
Year int,
Catagory varchar(100));

insert into movieNamesType3 values(1,'Anand',1971,'Top Rated India');
insert into movieNamesType3 values(2,'Dangal',2016,'Top Rated India');
insert into movieNamesType3 values(3,'Drishyam',2013,'Top Rated India');
insert into movieNamesType3 values(4,'Nayakan',1987,'Top Rated India');
insert into movieNamesType3 values(5,'Anbe Sivam',2003,'Top Rated India');

select * from movieNamesType3;


/* Top Rated Movies */

create table movieNamesType4(ID int primary key,
Title varchar(100) ,
Year int,
Catagory varchar(100));

insert into movieNamesType4 values(1,'Baazigar',1993,'Top Rated Movies');
insert into movieNamesType4 values(2,'24',2016,'Top Rated Movies');
insert into movieNamesType4 values(3,'Jodhaa Akbar',2008,'Top Rated Movies');
insert into movieNamesType4 values(4,'Wake Up Sid',2009,'Top Rated Movies');
insert into movieNamesType4 values(5,'Saala Khadoos',2016,'Top Rated Movies');

select * from movieNamesType4;




